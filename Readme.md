# Docker构建Confluence服务

## 目录结构

```bash
backups                     备份文件目录
confluence-data             Confluence数据目录
crack                       破解程序文件
docker-compose.yml          docker-compose
```

相关docker compose指令

* 构建容器: `docker-compose up -d`
* 停止compose容器：`docker-compose stop`
* 启动compose容器：`docker-compose start`

## 依赖准备

* docker
* docker compose
* mysql5.7

## 安装破解Confluence

在docker-compose.yml同级目录中，执行以下命令，创建并启动相关的容器：

```bash
# 后台模式启动
docker-compose up -d
```

启动成功后，通过浏览器 访问地址： http://localhost:28090

按照如下图操作：

![](images/1.png)
![](images/2.png)
![](images/3.png)

打开破解程序：

终端进入到crack目录，执行命令:

```bash
# 注意，-s 后的Server Id 请替换成自己的Server Id
java -jar atlassian-agent.jar -d -m test@test.com -n wiki -p conf -o https://wiki.test.com -s BL9T-VU2K-SRRJ-AIJ9
```

执行后，在终端会输出激活码，复制，直接粘贴就ok了

以下为插件破解示例， -p 后 填上 应用密钥，同样执行命令后，会输出激活码，复制粘贴即可
```
java -jar atlassian-agent.jar -p com.k15t.scroll.scroll-pdf -m aaa@bbb.com -n maomao -o https://wiki.test.com -s BXAU-XNSM-NEBN-A259
java -jar atlassian-agent.jar -p com.radiantminds.roadmaps-jira -m aaa@bbb.com -n maomao -o https://wiki.test.com -s BB21-7RC0-N9VP-D9XV
java -jar atlassian-agent.jar -p conf -m aaa@bbb.com -n maomao -o https://wiki.test.com -s BCJE-PM28-OW0Y-MK0W
```


破解程序提示成功后，会在crack同级目录看到多了一个文件叫`atlassian-extras-2.4.jar.bak`, 则代表成功，破解步骤还没完，请接着往下看。

将破解后的 jar 包导入到容器中，执行以下命令：

```bash
docker cp  ./atlassian-extras-2.4.jar jarvis_confluence:/opt/atlassian/confluence/confluence/WEB-INF/lib/atlassian-extras-decoder-v2-3.4.1.jar
```
![](images/10.png)


![](images/7.png)

安装破解完成，剩下的只是配置，自己按照提示做就Ok了。



数据库连接

jdbc:mysql://192.168.8.191:3306/confluence?charset=utf8&sessionVariables=transaction_isolation='READ-COMMITTED'&useSSL=false&serverTimezone=Asia/Shanghai&characterEncoding=utf8


## 番外

### pdf导出语言

如果没有安装导出语言，当导出pdf时，会出现中文乱码的情况。需要自行准备 ttf 的字体（可使用font目录下的字体）